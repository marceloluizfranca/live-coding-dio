import { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Button,
  FormControl,
  Stack,
} from "@mui/material";

export default function AddUser({
  open,
  onClose,
  onAddUser,
  onEditUser,
  currentUser,
}) {
  const [user, setUser] = useState({});

  useEffect(() => {
    setUser({
      name: currentUser.name || "",
      age: currentUser.age || "",
    });
  }, [currentUser]);

  function handleChange(event) {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  }

  function handleAddUser() {
    onAddUser({
      name: user.name,
      age: Number(user.age),
    });
  }

  function handleUpdateUser() {
    onEditUser(currentUser.id, {
      name: user.name,
      age: Number(user.age),
    });
  }

  console.log("USER => ", user);

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth="sm">
      <DialogTitle>{currentUser.id ? 'Editar usuário' : 'Novo usuário'}</DialogTitle>
      <DialogContent>
        <Stack spacing={2}>
          <DialogContentText>Não minta sua idade. 😜</DialogContentText>
          <FormControl>
            <TextField
              value={user.name}
              onChange={handleChange}
              name="name"
              autoFocus
              label="Nome"
              type="text"
              fullWidth
              variant="outlined"
            />
          </FormControl>
          <FormControl>
            <TextField
              value={user.age}
              onChange={handleChange}
              name="age"
              label="Idade"
              type="number"
              variant="outlined"
              inputProps={{ min: 1, max: 99 }}
            />
          </FormControl>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => onClose(false)}>Cancelar</Button>
        {currentUser.id ? (
          <Button onClick={handleUpdateUser}>Atualizar</Button>
        ) : (
          <Button onClick={handleAddUser}>Adicionar</Button>
        )}
      </DialogActions>
    </Dialog>
  );
}
