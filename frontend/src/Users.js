import { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Stack,
  Box,
  Button,
  Divider,
} from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import { red } from "@mui/material/colors";
import { API_URL } from "./constants";
import AddUser from "./AddUser";
import DeleteIcon from "@mui/icons-material/Delete";
import Host from "./Host";

export default function Users() {
  const [users, setUsers] = useState([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [currentUser, setCurrentUser] = useState({});

  function handleDialog(open) {
    if (open === false) {
      setCurrentUser({});
    }

    setIsDialogOpen(open);
  }

  function handleEdit(userId) {
    const user = users.find((user) => user.id === userId);

    setCurrentUser(user);
    handleDialog(true);
  }

  function addUser(user) {
    fetch(API_URL, {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("json", json);

        setUsers([...users, json]);
        handleDialog(false);
      });
  }

  function editUser(id, newUser) {
    fetch(`${API_URL}/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log("editUser", json);

        const editedUser = users.map((user) => {
          if (user.id === id) {
            return json;
          }

          return user;
        });

        console.log(editedUser);

        setUsers(editedUser);
        handleDialog(false);
      });
  }

  function deleteUser(id) {
    fetch(`${API_URL}/${id}`, {
      method: "delete",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        const newUsers = users.filter((user) => user.id !== id);

        setUsers(newUsers);
      });
  }

  useEffect(() => {
    fetch(API_URL)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        setUsers(json);
      });
  }, []);

  return (
    <>
      <Stack direction="row" alignItems="center" justifyContent="space-between">
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleDialog(true)}
        >
          Adicionar
        </Button>
        <Host />
      </Stack>
      <Divider sx={{ my: 2 }} />
      <Stack spacing={2}>
        {users.map((user) => {
          return (
            <Card sx={{ maxWidth: 345 }} key={user.id}>
              <CardHeader
                avatar={
                  <Avatar sx={{ bgcolor: red[500] }}>
                    {user.name ? user.name.split("")[0] : "?"}
                  </Avatar>
                }
                action={
                  <Stack direction="row">
                    <IconButton
                      aria-label="edit"
                      onClick={() => handleEdit(user.id)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton onClick={() => deleteUser(user.id)}>
                      <DeleteIcon />
                    </IconButton>
                  </Stack>
                }
                title={user.name}
                subheader={`${user.age} anos`}
              />
            </Card>
          );
        })}
      </Stack>
      <AddUser
        open={isDialogOpen}
        onClose={handleDialog}
        onAddUser={addUser}
        currentUser={currentUser}
        onEditUser={editUser}
      />
    </>
  );
}
