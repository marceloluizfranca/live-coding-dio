import { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Button,
  FormControl,
  Stack,
  Typography,
} from "@mui/material";
import { API_URL } from "./constants";

export default function Host() {
  const [host, setHost] = useState("");

  useEffect(() => {
    fetch(`${API_URL}/host`)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        console.log('host', json)

        setHost(json)
      });
  }, []);

  return <Typography fontWeight="bold" variant="body2" color="">app: v1 - host: {host}</Typography>;
}
