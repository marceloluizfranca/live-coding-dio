import { Container } from "@mui/material";
import Users from "./Users";
import AppBar from './AppBar';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#f5ec42', //default color #1F41CC
    },
  },
});

export default function App() {
  return (
    <ThemeProvider theme={theme}>

      <AppBar />
      <Container maxWidth="md" sx={{ mt: 12 }}>
        <Users />
      </Container>
    </ThemeProvider>
  );
}
