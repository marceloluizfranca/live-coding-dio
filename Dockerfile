FROM node:16.16.0-alpine as front

WORKDIR /app

COPY ./frontend/package*.json ./
COPY ./frontend/src ./src

RUN npm install

RUN npm run build

FROM node:16.16.0-alpine

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .
COPY --from=front /app/dist ./frontend/dist

EXPOSE 8000
CMD [ "node", "index.js" ]