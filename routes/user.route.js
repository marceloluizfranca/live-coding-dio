const express = require("express");
const router = express.Router();

const {
  getAllUsers,
  getHost,
  createUser,
  getOneUser,
  deleteUser,
  updateUser,
} = require("../controllers/user.controller");

router.get("/", getAllUsers);
router.get("/host", getHost);
router.get("/:id", getOneUser);
router.delete("/:id", deleteUser);
router.patch("/:id", updateUser);
router.post("/", createUser);

module.exports = router;
